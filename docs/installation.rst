============
Installation
============

At the command line::

    $ easy_install makesense

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv makesense
    $ pip install makesense