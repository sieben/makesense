===============================
makesense
===============================

.. image:: https://badge.fury.io/py/makesense.png
    :target: http://badge.fury.io/py/makesense
    
.. image:: https://travis-ci.org/sieben/makesense.png?branch=master
        :target: https://travis-ci.org/sieben/makesense

.. image:: https://pypip.in/d/makesense/badge.png
        :target: https://crate.io/packages/makesense?version=latest


makesense is a high-level framework for automating scientific experiments with a focus on Contiki related simulations.

* Free software: BSD license
* Documentation: http://makesense.rtfd.org.

Features
--------

* TODO