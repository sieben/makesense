#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys


try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist upload')
    sys.exit()

readme = open('README.rst').read()
history = open('HISTORY.rst').read().replace('.. :changelog:', '')

setup(
    name='makesense',
    version='0.1.0',
    description='makesense is a high-level framework for automating scientific experiments with a focus on Contiki related simulations.',
    long_description=readme + '\n\n' + history,
    author='Remy Leone',
    author_email='remy.leone@telecom-paristech.fr',
    url='https://github.com/sieben/makesense',
    packages=[
        'makesense',
    ],
    package_dir={'makesense': 'makesense'},
    include_package_data=True,
    install_requires=[
    ],
    license="BSD",
    zip_safe=False,
    keywords='makesense',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
    ],
    test_suite='nose.collector',
)
